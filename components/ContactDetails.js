import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Button,
  TouchableOpacity,
} from 'react-native';
import {callNumber} from '../helpers/callNumber';
import {sendEmail} from '../helpers/sendEmail';

function ContactDetails({route, navigation}) {
  const {id, firstName, lastName, phone, email, avatar} = route.params;

  const callHandler = () => callNumber(phone);

  const emailHandler = () => sendEmail(email);

  const deleteHandler = () =>
    navigation.navigate({
      name: 'Contacts',
      params: {deleteId: id},
      merge: true,
    });

  return (
    <View style={styles.container}>
      <Image
        style={styles.avatar}
        source={{
          uri: `${avatar}`,
        }}
      />
      <Text style={styles.name}>{`${firstName} ${lastName}`}</Text>
      <DetailsField title="Phone" content={phone} handler={callHandler} />
      <DetailsField title="Email" content={email} handler={emailHandler} />
      <View style={styles.buttonsContainer}>
        <Button
          color="#b22222"
          title="Delete"
          onPress={() => deleteHandler()}
        />
        <Button color="#2e8b57" title="Call" onPress={() => callHandler()} />
      </View>
    </View>
  );
}

function DetailsField({title, content, handler}) {
  return (
    <View style={styles.fieldContainer}>
      <TouchableOpacity onPress={() => handler()}>
        <View>
          <Text style={styles.fieldTitle}>{title}</Text>
          <Text>{content}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingTop: 20,
  },
  avatar: {
    height: 170,
    width: 170,
    backgroundColor: 'white',
    borderRadius: 100,
  },
  name: {
    fontSize: 24,
    fontWeight: '700',
    marginTop: 15,
    marginBottom: 20,
  },
  fieldContainer: {
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 6,
    paddingVertical: 8,
    paddingHorizontal: 12,
    marginBottom: 10,
  },
  fieldTitle: {
    fontWeight: '700',
    fontSize: 16,
  },
  buttonsContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 10,
  },
});

export default ContactDetails;
