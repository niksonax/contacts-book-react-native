import {PermissionsAndroid} from 'react-native';
import Contacts from 'react-native-contacts';

export const getContacts = async () => {
  PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS);

  // App crashes at start if execute this code
  Contacts.getAll()
    .then(contacts => console.log(contacts))
    .catch(err => console.log(err));
};
