import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

function Contact(props) {
  const {firstName, lastName, phone, avatar} = props.contact;

  return (
    <View style={styles.contact}>
      <Image
        style={styles.avatar}
        source={{
          uri: `${avatar}`,
        }}
      />
      <View style={styles.contactBody}>
        <Text style={styles.name}>{`${firstName} ${lastName}`}</Text>
        <Text style={styles.phone}>{phone}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  contact: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 6,
    paddingBottom: 6,
    marginBottom: 12,
    backgroundColor: 'white',
    borderRadius: 6,
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginRight: 15,
  },
  name: {
    fontWeight: '700',
  },
  phone: {
    color: '#666',
  },
});

export default Contact;
