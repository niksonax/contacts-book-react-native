import {Linking} from 'react-native';

export async function sendEmail(to) {
  let url = `mailto:${to}`;
  const canOpen = await Linking.canOpenURL(url);
  if (!canOpen) {
    throw new Error('Provided URL can not be handled');
  }

  return Linking.openURL(url);
}
