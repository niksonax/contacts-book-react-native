import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import ContactsPage from './components/ContactsPage';
import ContactDetails from './components/ContactDetails';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Contacts">
        <Stack.Screen
          options={{headerShown: false}}
          name="Contacts"
          component={ContactsPage}
        />
        <Stack.Screen name="Contact Details" component={ContactDetails} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
