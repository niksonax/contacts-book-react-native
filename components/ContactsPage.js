import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  TextInput,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Contact from './Contact';

import contactsMockData from '../assets/contactsData';

function ContactsPage({route, navigation}) {
  const [contactsData, setContactsData] = React.useState(contactsMockData);
  const [contacts, setContacts] = React.useState(contactsData);
  const [input, setInput] = React.useState('');

  const deleteId = route.params?.deleteId;

  React.useEffect(() => {
    const deleteContact = id => {
      const filteredContacts = contactsData.filter(
        contact => contact.id !== id,
      );
      setContactsData(filteredContacts);
      setContacts(contactsData);
    };

    if (deleteId) {
      deleteContact(deleteId);
    }
  }, [deleteId]);

  const filterContacts = text => {
    setInput(text);

    const filteredContacts = contactsData.filter(contact => {
      const contactName = `${contact.firstName.toLowerCase()} ${contact.lastName.toLowerCase()}`;
      const filterName = text.toLowerCase();
      return contactName.includes(filterName);
    });

    setContacts(filteredContacts);
  };

  const renderContact = ({item}) => (
    <TouchableOpacity
      onPress={() => navigation.navigate('Contact Details', item)}>
      <Contact contact={item} />
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.searchbarContainer}>
        <TextInput
          value={input}
          onChangeText={text => filterContacts(text)}
          style={styles.searchbar}
        />
      </View>

      <FlatList
        data={contacts}
        renderItem={renderContact}
        keyExtractor={item => item.id}
        style={styles.list}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  searchbarContainer: {
    height: 55,
    backgroundColor: 'whitesmoke',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,
    elevation: 11,
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchbar: {
    height: 37,
    width: '85%',
    backgroundColor: 'white',
    borderRadius: 4,
  },
  list: {
    paddingHorizontal: 10,
    paddingVertical: 8,
  },
});

export default ContactsPage;
